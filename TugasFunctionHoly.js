console.log('Tugas Function Modul 1');
console.log('==================');

/*
    Indha Suci Ramadani
    2018 120 042
*/

// No 1

function teriak() {
    return ("Halo Sanbers!")
}

console.log(teriak());

console.log('==================');

// No 2

function hasilkali() {
    return (ang1*ang2)
}

var ang1 = 12;
var ang2 = 4;

console.log(hasilkali());

console.log('==================');

// No 3

function introduce (){
    return (perkenalan)
}

var name    = "Agus"
var age     = "30"
var address = "Jln.Malioboro, Yogyakarya"
var hobby   = "Gaming"

var perkenalan = ('Nama saya ' + name + ', umur saya' + " " +  age + " " + 'tahun, ' + 'alamat saya di ' + address + ',' + 'dan saya punya hobby yaitu' + " " + hobby + '!');

console.log(perkenalan);