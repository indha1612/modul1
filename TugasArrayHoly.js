console.log('Tugas Array Modul 1');
console.log('==================');

/*
    Indha Suci Ramadani
    2018 120 042
*/

//No 1 Array
console.log("No 1");

function range(startNum, finishNum){
    let a = startNum;
    let b = finishNum;
    let c = 0;
    let Num = [];
    let createFunc = (n) => {return n};
    if(a<b) {
        for(i=a; i <= b; i++){
            c += i;
            Num.push(i);
            createFunc(c);

        }
        return Num;
    
    }else if (a>b){
        for(i=a; i>=b; i--){
            c += i;
            Num.push(i);
            Num.sort(function i (a, b) { return b-a});
            createFunc(c);
        }
        return Num;
    }else if(a === undefined && b === undefined ) {
        return -1;
    }else if( b === undefined ) {
        return -1;
    }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

// No 2 Array
console.log("==========================");
console.log("No 2");

function rangeWithStep(startNum, finishNum, Step){
    let a = startNum;
    let b = finishNum;
    let c = Step;
    let d = 0;
    let Num = [];
    let checkStep = c === undefined ? 1 : c;
    let createFunc = (n) => {return n};
    if(a<b) {
        for(i=a; i <= b; i+=checkStep){
            d += i;
            Num.push(i);
            createFunc(d);

        }
        return Num;
    }else if (a>b){
        for(i=a; i>=b; i-=checkStep){
            d += i;
            Num.push(i);
            Num.sort(function i (a, b, c) { return c-b-a});
            createFunc(d);
        }
        return Num;
    }   
}


console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// No 3 Array
console.log("==========================");
console.log("No 3");

function sum(startNum, finishNum, step) {
    let a = startNum;
    let b = finishNum;
    let c = step;
    let d = 0;
    let checkStep = c === undefined ? 1 : c;
    let createFunc = (n) => {return n};
        if(a < b) {
            for(i = a; i <= b; i+=checkStep){
                d += i;
                createFunc(d);
            }
            return d;
        } else if(a > b) {
            for(i = a; i >= b; i-=checkStep){
                d += i;
                createFunc(d);
            }
            return d;
        } else if(a === undefined && b === undefined && c === undefined) {
            return 0;
        } else if(b === undefined && c === undefined) {
            return 1;
        }
}
console.log(sum(1, 10));
console.log('==============');
console.log(sum(5, 50, 2));
console.log('==============');
console.log(sum(15, 10));
console.log('==============');
console.log(sum(20, 10, 2));
console.log('==============');
console.log(sum(1));
console.log('==============');
console.log(sum());

// No 4
console.log("==========================");
console.log('NO 4')

function dataHandling(arr) {
    let x = [];
    for(a = 0; a < arr.length; a++){
        x.push({
            Nomor : arr[a][0],
            Nama : arr[a][1],
            TTL : arr[a][2]+' '+ arr[a][3],
            Hobi : arr[a][4]
        });
    }
    for (let item of x) {
        console.log(
            `Nomor ID : ${item.Nomor}\nNama : ${item.Nama}\nTTL : ${item.TTL}\nHobi : ${item.Hobi}\n`
        );
    }
}
const input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
dataHandling(input);

// No 5 
console.log("==========================");
console.log('NO 5')

function reverseString(str) {
    var currentString = str;
    var newString = '';
   for (let i = str.length - 1; i >= 0; i--) {
     newString = newString + currentString[i];
    }
    
    return newString;
   }
console.log(reverseString("Kasur Rusak")) // kasuR rusaK
console.log(reverseString("SanberCode")) // edoCrebnaS
console.log(reverseString("Haji Ijah")) // hajI ijaH
console.log(reverseString("racecar")) // racecar
console.log(reverseString("I am Sanbers")) // srebnaS ma I

// No 6 Array
console.log("==========================");
console.log('NO 6')

function dataHandling2(arr){
    //teks array 1
    let teksAkhir = arr[1].concat("Elsharawy");
    arr.splice(1, 1, teksAkhir);
    //teks array 2
    let teksAwal = arr[2].split(" ");
    teksAwal.unshift("Provinsi");
    let gabungTeks = teksAwal.join(" ");
    arr.splice(2, 1, gabungTeks);
    //teks array 4
    arr.splice(4, 1, "Pria", "SMA Internasional Metro");
    //modifikasi tanggal bulan tahun
    let spTanggal = arr[3].split("/");
    let sbst = spTanggal[1].substring(1);
    let bulan = Number(sbst);
    switch(bulan) {
        case 1:
            bulan = "Januari";
        break;
        case 2:
            bulan = "Februari";
        break;
        case 3:
            bulan = "Maret";
        break;
        case 4:
            bulan = "April";
        break;
        case 5:
            bulan = "Mei";
        break;
        case 6:
            bulan = "Juni";
        break;
        case 7:
            bulan = "Juli";
        break;
        case 8:
            bulan = "Agustus";
        break;
        case 9:
            bulan = "September";
        break;
        case 10:
            bulan = "Oktober";
        break;
        case 11:
            bulan = "November";
        break;
        case 12:
            bulan = "Desember";
        break;
    }
    // array tanggal
    let arrTanggal = spTanggal[2]+' '+spTanggal[0]+' '+spTanggal[1];
    let splTanggal = arrTanggal.split(' ');
    // gabungkan tanggal
    let gbTanggal = spTanggal.join('-');
    //pembatasan nama 15 karakter
    let slNama = arr[1].slice(0, 14);

    console.log(arr);
    console.log("=================");
    console.log(bulan);
    console.log("=================");
    console.log(splTanggal);
    console.log("=================");
    console.log(gbTanggal);
    console.log("=================");
    console.log(slNama);
}
const input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);
