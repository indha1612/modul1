console.log('Tugas Looping Modul 1');
console.log('==================');

/*
    Indha Suci Ramadani
    2018 120 042
*/

// No 1
console.log('No 1');

var judul = "Loop 1!"
    console.log(judul)

var nilai =0;
while(nilai<=18)  {
    nilai+=2;
    console.log(nilai + '- I love coding' );
    
}

var judul = "Loop 2!"
    console.log(judul)

    var nilai =22;
    while(nilai>2)  {
        nilai-=2;
        console.log(nilai + '- I will become a mobile developer' );
    }


    // No 2
    console.log('================================');
    console.log('No 2');
    
    for(let angka = 1; angka <= 20; angka++){
        if ((angka%2)===0) {
          console.log(angka + ' - Berkualitas');
        } if (angka%3 === 0 && angka%2 !== 0) {
          console.log(angka + ' - I Love Coding');
        } else if((angka%2)===1){
          console.log(angka + ' - Santai');
        }
      }

    // No 3
    console.log('================================');
    console.log('No 3');
    
       var s = '';
        for(i = 0; i < 4; i++){
            for(j =0; j < 8; j++){
                s+='#';
    }
    s+= '\n';
}
console.log(s);

// No 4
console.log('================================');
console.log('No 4');

var s = '';
for(i = 0; i < 7; i++){
    for(j =0; j <= i; j++){
        s+='#';
    }
    s+= '\n';
}
console.log(s);

// No 5
console.log('================================');
console.log('No 5');

var s = '';
var lebar = 8;
var panjang = 8;
for(var i = 1; i <= panjang; i++){
    if(i %2 ==0) {

        for(var j = 1; j <= lebar; j++){
            if(j%2 == 0){
                s+=' # '; 
            }else{
                s+= ' ';
            }
            
            
    }
    
    }else{
        for(var j = 1; j <= lebar; j++){
            if(j%2 ==0){
                s+=' '; 
            }else{
                s+= ' # ';
            }

        }

    }
    s += '\n';
}

console.log(s);
